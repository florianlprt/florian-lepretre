# Import des modules necessaires
import turtle
import time
import random

# Definition de la zone de jeu
screen = turtle.Screen()
screen.tracer(0) 

# TODO-1 : Definir le titre, la couleur de fond, la taille de la fenetre

# TODO-2 : Definir le vaisseau (rocket) avec sa taille, position, couleur...

# TODO-3 : Definir le dictionnaire pour l'etat du jeu
# TODO-3 : Definir l'affichage du score

# TODO-4 : Definir une liste de 10 elements Turtle (10 asteroides)
# TODO-4 : Definir la couleur, forme, position aleatoire de chaque asteroide

# TODO-6 : Gerer les evenements clavier

# TODO-5 : Definir une fonction pour deplacer les asteroides
# TODO-9 : Detecter la collision avec le vaisseau
# TODO-9 : Mettre a jour les scores

# TODO-7 : Definir une fonction pour deplacer le vaisseau
# TODO-8 : Detecter la collision avec le bord superieur
# TODO-8 : Mettre a jour les scores

# Boucle d'affichage
while True:
  screen.update()

  # TODO-5 : Mettre a jour les asteroides (executer la fonction move_asteroids)
  # TODO-7 : Mettre a jour le vaisseau (executer la fonction move_rocket)

  time.sleep(0.02)

# Demarrer Turtle
screen.mainloop()