function makeLink(id, tag, text) {
  const link = document.createElement('a');
  link.classList.add(`link-${tag}`, 'nav-link');
  link.href = `#${id}`;
  link.innerText = text;
  return link;
}

function makeNav() {
  const nav = document.createElement('nav');
  nav.classList.add('nav');
  return nav;
}

function makeEntry(toc, nav, subnav) {
  if (subnav.children.length) {
    nav.appendChild(subnav);
  }
  toc.appendChild(nav);
}

function makeTOC(titles, toc) {
  // Extract data
  const data = Array.from(titles).map(
    (element, index) => ({
      id: element.id || `toc${index}`,
      tag: element.tagName.toLowerCase(),
      text: element.innerText,
      html: element.innerHTML,
      element
    }));

  // Reform HTML
  data.forEach(({ id, html, element }) => {
    element.id = id;
    element.innerHTML = `<a href="#${id}">${html}</a>`;
  });

  // Create TOC entries
  let nav;
  let subnav;
  data.forEach(({ id, tag, text }, index) => {
    if (tag === 'h3') {
      if (index !== 0) {
        makeEntry(toc, nav, subnav);
      }
      nav = makeNav();
      subnav = makeNav();
      nav.appendChild(makeLink(id, tag, text));
    }
    else if (tag === 'h4') {
      subnav.appendChild(makeLink(id, tag, text));
    }
  });
  makeEntry(toc, nav, subnav);
}

makeTOC(
  document.querySelectorAll('#lesson article h3, #lesson article h4'),
  document.querySelector('#toc')
);