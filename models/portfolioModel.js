const portfolio = {

  ai: {
    title: 'Artificial Intelligence',
    items: [
      { 
        title: '🖼️ EvolGAN',
        description: '\
          <em>EvolGAN</em> is a prototype web application where you can\
          generate pictures of humans with GANs. The user interface allows you\
          to select some pictures in order to generate a new set of pictures\
          that share the characteristics of the selected ones.',
        video: '/videos/portfolio/evolgan.mp4',
        more: '\
          Try the <a href="https://evolgan.herokuapp.com/">app</a> (prototype).'
      },
      { 
        title: '🏆 ez.gg',
        description: '\
          <em>ez.gg</em> is a prototype web platform where you can predict the\
          issue of a League of Legends match, even before it starts. It\
          combines a simple and clear user interface for the display and\
          interpretation of predictions. Predictions are made by a machine\
          learning model, trained on hundreds of thousands of games\
          (provided by Riot Games Big Data APIs).',
        video: '/videos/portfolio/ezgg.mp4',
        more: '\
          Try the <a href="https://ez-gg.herokuapp.com/">app</a> (prototype).'
      },
      { 
        title: '🎲 ai.io',
        description: '\
          <em>ai.io</em> is a prototype web platform where you can play board\
          games with your friends or against artificial intelligence (AI)\
          programs. The latter are essentially developped by the community. The\
          leaderboards of the best AIs are regularly updated.',
        video: '/videos/portfolio/aiio.mp4',
        more: '\
          See <a href="https://gitlab.com/florianlprt/ai.io">GitLab repository</a>.'
      },
      { 
        title: '🚍 SIALAC Optibus',
        description: '\
          <em>SIALAC Optibus</em> is a Python project that implements algorithms for\
          bus stops spacing optimization problems based on the SIALAC Benchmark. For\
          more details, please read\
          <em>Combinatorial Surrogate-Assisted Optimization for Bus Stops Spacing\
          Problems</em>, published in <em>Artificial Evolution Proceedings,\
          2019</em>.',
        image: '/images/portfolio/poster_optibus.png',
        more: '\
          See <a href="https://gitlab.com/florianlprt/sialac_optibus">GitLab repository</a>\
          and my <a href="/research">research</a>.'
      },
      { 
        title: '🚦 SIALAC Optisig',
        description: '\
          <em>SIALAC Optisig</em> is a Python project that implements algorithms for\
          traffic signal controls optimization, based on the SIALAC Benchmark. For\
          more details, please read <em>Fitness landscapes analysis and adaptive\
          algorithms design for traffic lights optimization on SIALAC\
          benchmark</em>, published in <em>Applied Soft Computing, 2019</em>.',
        youtube: 'https://www.youtube.com/embed/jugQtzu0Kx0',
        more: '\
          See <a href="https://gitlab.com/florianlprt/sialac_optisig">GitLab repository</a>\
          and my <a href="/research">research</a>.'
      },
      { 
        title: '📐 UCBature',
        description: '\
          <em>UCBature</em> is a Julia implementation of the numerical integration\
          algorithm published in the <em>Conference on Technologies and Applications\
          of Artificial Intelligence</em>. For more details, please read\
          <em>Multi-armed bandit for stratified sampling: Application to numerical\
          integration</em>.',
        image: 'https://github.com/florianLepretre/ucbature/raw/master/examples/plot_integrands.png',
        more: '\
          See <a href="https://github.com/florianLepretre/ucbature">GitHub repository</a>\
          and my <a href="/research">research</a>.'
      },
      { 
        title: '🧮 Walsh Surrogate-assisted Optimization',
        description: '\
          <em>Walsh Surrogate-assisted Optimization (WSaO)</em> is the first\
          surrogate-assisted optimization algorithm based on the mathematical\
          foundations of discrete Walsh functions, combined with the powerful\
          grey-box optimization techniques in order to solve pseudo-boolean\
          optimization problems. For more details, please read <em>Walsh functions\
          as surrogate model for pseudo-boolean optimization problems</em>,\
          published in <em>The Genetic and Evolutionary Computation Conference\
          (GECCO), 2019</em>.',
        more: '\
          See <a href="https://gitlab.com/florianlprt/wsao">GitLab repository</a>\
          and my <a href="/research">research</a>.'
      }
    ]
  },

  games: {
    title: 'Games',
    items: [
      { 
        title: '🐔 BattleSheep AR',
        description: '\
          <em>BattleSheep - Augmented Reality Edition</em> is a mobile augmented\
          reality game made with <a href="https://unity.com/">Unity</a> and <a\
          href="https://developer.vuforia.com/">Vuforia</a>. The game has been\
          developped in tandem with <a\
          href="http://amaury-dubois.herokuapp.com/">Amaury Dubois</a>.',
        youtube: 'https://www.youtube.com/embed/g-iD22FakA0',
      },
      { 
        title: '🏐 Game of Bowls',
        description: '\
          <em>Game of Bowls</em> is a virtual reality simulator of the French\
          <a href="https://en.wikipedia.org/wiki/P%C3%A9tanque">pétanque</a>, made\
          with <a href="https://unity.com/">Unity</a> and <a\
          href="https://en.wikipedia.org/wiki/Leap_Motion">Leap Motion</a>. The game\
          has been developped in tandem with <a\
          href="http://amaury-dubois.herokuapp.com/">Amaury Dubois</a>.',
        youtube: 'https://www.youtube.com/embed/j8lGAe1g-Jo',
      }
    ]
  },

  bigdata: {
    title: 'Knowledge Management & Big Data',
    items: [
      { 
        title: '🏙 Sky Scraper',
        description: '\
          <em>Sky Scraper</em> is a Chromium bot that collects countless satellite\
          images from GoogleMaps (made with <a\
          href="https://github.com/puppeteer/puppeteer">Puppeteer</a>).',
        video: 'https://gitlab.com/florianlprt/sky-scraper/-/raw/master/public/videos/skyscraper.mp4',
        more: '\
          See <a href="https://gitlab.com/florianlprt/sky-scraper">GitLab repository</a>.'
      },
      { 
        title: '📋 Taggle',
        description: '\
          <em>Taggle</em> is a fast and intuitive tool designed to manually tag\
          large image datasets used in machine learning and classification problems\
          or <a href="https://www.kaggle.com/">Kaggle</a> challenges.',
        video: 'https://gitlab.com/florianlprt/taggle/-/raw/master/public/videos/taggle.mp4',
        more: '\
          See <a href="https://gitlab.com/florianlprt/taggle">GitLab repository</a>.'
      },
      { 
        title: '⚔️ LoL - MDR',
        description: '\
          <em>League of Legends - Match Data Requests</em> crawls <a\
          href="https://youtu.be/KTiAH_nIFtE?t=75">tons</a> of data about\
          <a target="_blank" href="https://euw.leagueoflegends.com/fr-fr/">League of\
          Legends</a> matches, for knowledge management purpose.',
        image: '/images/portfolio/poster_lolmdr.png',
        more: '\
          See <a href="https://gitlab.com/florianlprt/lol-mdr">GitLab repository</a>.'
      }
    ]
  },

  simulation: {
    title: 'Modeling and Simulation',
    items: [
      { 
        title: '🚘 SIALAC Benchmark',
        description: '\
          Scenario Investigation of Agents Localisation for Algorithm Conception is\
          a Python generator for <a href="https://www.matsim.org/">MATSim</a>\
          benchmarks. For more details, please read\
          <em>SIALAC Benchmark: On the design of adaptive algorithms for traffic\
          lights problems</em>, published in <em>The Genetic and Evolutionary\
          Computation Conference (GECCO), 2019</em>.',
        image: 'https://gitlab.com/florianlprt/cst-2018/-/raw/master/slides/medias/sialac.png',
        more: '\
          See <a href="https://gitlab.com/florianlprt/sialac_benchmark">GitLab\
          repository</a> and my <a href="/research">research</a>.'
      },
      { 
        title: '🌫 Gam\'Air',
        description: '\
          <em>Gam\'Air</em> models pollution with <a\
          href="https://gama-platform.github.io/">Gama</a> in two French cities :\
          Calais and Dunkerque.',
        image: '/images/portfolio/poster_gamair.png',
        more: '\
          See <a href="https://gitlab.com/florianlprt/gamair">GitLab\
          repository</a>.'
      },
      { 
        title: '🐍 Gampy',
        description: '\
          <em>Gampy</em> is a Python module that permits to run <a\
          href="https://gama-platform.github.io/">Gama</a> simulations from Python\
          scripts.',
        more: '\
          See <a href="https://gitlab.com/florianlprt/gampy">GitLab\
          repository</a>.'
      }
    ]
  },

  misc: {
    title: 'Miscellaneous',
    items: [
      {
        title: '🔮 League of Cheatsheet',
        description: '\
          <a href="https://florianlprt.gitlab.io/league-of-cheatsheet/">League \
          of Cheatsheet</a> is a cheatsheet generator for\
          <a target="_blank" href="https://euw.leagueoflegends.com/fr-fr/">\
          League of Legends</a> champions.',
        more: '\
          See <a href="https://gitlab.com/florianlprt/league-of-cheatsheet">\
          GitLab repository</a>.'
      }
    ]
  },

};

module.exports = { portfolio };
