const courses = {

  projetweb: {
    sujet: { id: 'sujet', route: '/teaching/projetweb/sujet', title: 'Développement d\'une boutique e-commerce', view: 'projetweb/sujet' },
    tutogit: { id: 'tutogit', route: '/teaching/projetweb/tutogit', title: 'Tutoriel Git', view: 'projetweb/tutogit' },
    tutodeploy: { id: 'tutodeploy', route: '/teaching/projetweb/tutodeploy', title: 'Déploiement du projet', view: 'projetweb/tutodeploy' }
  },

  info1: {
    condense: { id: 'condense', route: '/teaching/info1/condense', title: 'Condensé du cours', view: 'info1/condense' },
    listes: { id: 'listes', route: '/teaching/info1/listes', title: 'Listes', view: 'info1/listes' },
    revisions: { id: 'revisions', route: '/teaching/info1/revisions', title: 'Révisions', view: 'info1/revisions' },
    projet1: { id: 'projet1', route: '/teaching/info1/projet1', title: 'Projet - Space Race', view: 'info1/projet1' }
  },

  web: {
    intro: { id: 'intro', route: '/teaching/web/intro', title: 'Introduction', view: 'web/intro' },
    html: { id: 'html', route: '/teaching/web/html', title: 'Le langage HTML', view: 'web/html' },
    css: { id: 'css', route: '/teaching/web/css', title: 'Le langage CSS', view: 'web/css' },
    bootstrap: { id: 'bootstrap', route: '/teaching/web/bootstrap', title: 'Bootstrap', view: 'web/bootstrap' },
    javascript: { id: 'javascript', route: '/teaching/web/javascript', title: 'Le langage JavaScript', view: 'web/javascript' },
    nodejs: { id: 'nodejs', route: '/teaching/web/nodejs', title: 'Applications Node.js', view: 'web/nodejs' },
    tp1: { id: 'tp1', route: '/teaching/web/tp1', title: 'TP1 - Découverte de HTML et CSS', view: 'web/tp1', sources: '/downloads/web/tp1.zip' },
    tp2: { id: 'tp2', route: '/teaching/web/tp2', title: 'TP2 - Découverte de Bootstrap', view: 'web/tp2', sources: '/downloads/web/tp2.zip' },
    tp3: { id: 'tp3', route: '/teaching/web/tp3', title: 'TP3 - Design de l\'application e-commerce', view: 'web/tp3', sources: '/downloads/web/tp3.zip' },
    tp4: { id: 'tp4', route: '/teaching/web/tp4', title: 'TP4 - Des pages dynamiques avec JavaScript', view: 'web/tp4', sources: '/downloads/web/tp4.zip' },
    tp5: { id: 'tp5', route: '/teaching/web/tp5', title: 'TP5 - Un formulaire intéractif', view: 'web/tp5', sources: '/downloads/web/tp5.zip' },
    tp6: { id: 'tp6', route: '/teaching/web/tp6', title: 'TP6 - Découverte de node.js et express', view: 'web/tp6', sources: '/downloads/web/tp6.zip' },
    tp7: { id: 'tp7', route: '/teaching/web/tp7', title: 'TP7 - Contrôleurs, vues, et templates', view: 'web/tp7', sources: '/downloads/web/tp7.zip' },
    tp8: { id: 'tp8', route: '/teaching/web/tp8', title: 'TP8 - Liaison avec SQLite', view: 'web/tp8', sources: '/downloads/web/tp8.zip' },
    tp9: { id: 'tp9', route: '/teaching/web/tp9', title: 'TP9 - Espace client', view: 'web/tp9' },
    tp10: { id: 'tp10', route: '/teaching/web/tp10', title: 'TP10 - Requêtes AJAX', view: 'web/tp10', sources: '/downloads/web/tp10.zip' },
    tp11: { id: 'tp11', route: '/teaching/web/tp11', title: 'TP11 - Un chat en temps-réel avec socket.io', view: 'web/tp11', sources: '/downloads/web/tp11.zip' },
    tp12: { id: 'tp12', route: '/teaching/web/tp12', title: 'TP12 - Un jeu temps-réel avec socket.io', view: 'web/tp12', sources: '/downloads/web/tp12.zip' },
    tutonode: { id: 'tutonode', route: '/teaching/web/tutonode', title: 'Installation de Node.js', view: 'web/tutonode' },
    examen: { id: 'examen', route: '/teaching/web/examen', title: 'Examen', view: 'web/examen', pass: 'lego-3x4m3N/', sources: '/private/web/examen/lego-webstore-exam.zip' }
  },

  webservices: {
    intro: { id: 'intro', route: '/teaching/webservices/intro', title: 'Introduction', view: 'webservices/intro' },
    xml: { id: 'xml', route: '/teaching/webservices/xml', title: 'Le langage XML', view: 'webservices/xml' },
    dtd: { id: 'dtd', route: '/teaching/webservices/dtd', title: 'Document Type Definition (DTD)', view: 'webservices/dtd' },
    xsd: { id: 'xsd', route: '/teaching/webservices/xsd', title: 'XML Schema Definition (XSD)', view: 'webservices/xsd' },
    xpath: { id: 'xpath', route: '/teaching/webservices/xpath', title: 'XML Path Language (XPath)', view: 'webservices/xpath' },
    xslt: { id: 'xslt', route: '/teaching/webservices/xslt', title: 'Transformations XSL (XSLT)', view: 'webservices/xslt' },
    svg: { id: 'svg', route: '/teaching/webservices/svg', title: 'Scalable Vector Graphics (SVG)', view: 'webservices/svg' },
    json: { id: 'json', route: '/teaching/webservices/json', title: 'JavaScript Object Notation (JSON)', view: 'webservices/json' },
    tp1: { id: 'tp1', route: '/teaching/webservices/tp1', title: 'TP1 - Découverte de XML', view: 'webservices/tp1' },
    tp2: { id: 'tp2', route: '/teaching/webservices/tp2', title: 'TP2 - Validation avec DTD', view: 'webservices/tp2' },
    tp3: { id: 'tp3', route: '/teaching/webservices/tp3', title: 'TP3 - Validation avec XSD', view: 'webservices/tp3' },
    tp4: { id: 'tp4', route: '/teaching/webservices/tp4', title: 'TP4 - Expressions XPath', view: 'webservices/tp4' },
    tp5: { id: 'tp5', route: '/teaching/webservices/tp5', title: 'TP5 - Transformations XSL', view: 'webservices/tp5', sources: '/downloads/webservices/node/xml-server.zip' },
    tp6: { id: 'tp6', route: '/teaching/webservices/tp6', title: 'TP6 - SVG', view: 'webservices/tp6', sources: '/downloads/webservices/node/xml-server-2.zip' },
    tp7: { id: 'tp7', route: '/teaching/webservices/tp7', title: 'TP7 - API REST (1)', view: 'webservices/tp7' },
    tp8: { id: 'tp8', route: '/teaching/webservices/tp8', title: 'TP8 - API REST (2)', view: 'webservices/tp8' },
    tp9: { id: 'tp9', route: '/teaching/webservices/tp9', title: 'TP9 - API REST (3)', view: 'webservices/tp9' },
    projet: { id: 'projet', route: '/teaching/webservices/projet', title: 'Projet', view: 'webservices/projet', sources: '/downloads/webservices/node/xml-server-projet.zip' },
    tp23corriges: { id: 'tp23corriges', route: '/downloads/webservices/corriges/tp23-dtd-xsd.zip', title: 'Corrigés des TP2 et TP3' },
    tp4corriges: { id: 'tp4corriges', route: '/downloads/webservices/corriges/tp4-xpath.zip', title: 'Corrigés du TP4' },
    tp567corriges: { id: 'tp567corriges', route: '/downloads/webservices/corriges/tp567-xml-server.zip', title: 'Corrigés des TP5, TP6 et TP7' },
    tp8corriges: { id: 'tp8corriges', route: '/downloads/webservices/corriges/tp8.zip', title: 'Corrigés du TP8' },
    tp9corriges: { id: 'tp9corriges', route: '/downloads/webservices/corriges/tp9.zip', title: 'Corrigés du TP9' }
  },

  gal: {
    projet: { id: 'projet', route: '/teaching/gal/projet', title: 'Projet GAL', view: 'gal/projet' }
  },

  projetl3: {
    consignes: { id: 'consignes', route: '/teaching/projetl3/consignes', title: 'Consignes', view: 'projetl3/consignes' },
    cours: { id: 'cours', route: '/teaching/projetl3/cours', title: 'Cours vidéo', view: 'projetl3/cours' },
    promo2020: { id: 'promo2020', route: '/teaching/projetl3/promo2020', title: 'Promotion 2019-2020', view: 'projetl3/promo2020' }
  }

};

const teachings = {

  projetweb: {
    title: '🌐 Projets Web',
    students: 'L2 Informatique',
    year: '2020 - 2021',
    resources: [
      { category: 'Projet', contents: [
        courses.projetweb.sujet
      ] },
      { category: 'Tutoriels', contents: [
        courses.projetweb.tutogit,
        courses.projetweb.tutodeploy
      ] }
    ]
  },

  info1: {
    title: '🐍 Info1',
    students: 'L1 MSPI',
    year: '2020 - 2021',
    resources: [
      { category: 'Cours', contents: [
        courses.info1.condense
      ] },
      { category: 'Exercices', contents: [
        courses.info1.listes,
        courses.info1.revisions
      ] },
      { category: 'Projets', contents: [
        courses.info1.projet1
      ] }
    ]
  },

  web: {
    title: '🌍 Développement Web',
    students: 'L2 Informatique',
    year: '2019 - 2020',
    resources: [
      { category: 'Cours', contents: [
        courses.web.intro,
        courses.web.html,
        courses.web.css,
        courses.web.bootstrap,
        courses.web.javascript,
        courses.web.nodejs ] },
      { category: 'TP', contents: [
        courses.web.tp1,
        courses.web.tp2,
        courses.web.tp3,
        courses.web.tp4,
        courses.web.tp5,
        courses.web.tp6,
        courses.web.tp7,
        courses.web.tp8,
        courses.web.tp9,
        courses.web.tp10 ] },
      { category: 'Extras', contents: [
        courses.web.tp11,
        courses.web.tp12] },
      { category: 'Tutoriels', contents: [
        courses.web.tutonode ] }
    ]
  },

  webservices: {
    title: '📡 Web Services',
    students: 'M1 Apprentissage',
    year: '2019 - 2020',
    resources: [
      { category: 'Cours', contents: [
        courses.webservices.intro,
        courses.webservices.xml,
        courses.webservices.dtd,
        courses.webservices.xsd,
        courses.webservices.xpath,
        courses.webservices.xslt,
        courses.webservices.svg,
        courses.webservices.json ] },
      { category: 'TP', contents: [
        courses.webservices.tp1,
        courses.webservices.tp2,
        courses.webservices.tp3,
        courses.webservices.tp4,
        courses.webservices.tp5,
        courses.webservices.tp6,
        courses.webservices.tp7,
        courses.webservices.tp8,
        courses.webservices.tp9 ] },
      { category: 'Corrigés', contents: [
        courses.webservices.tp23corriges,
        courses.webservices.tp4corriges,
        courses.webservices.tp567corriges,
        courses.webservices.tp8corriges,
        courses.webservices.tp9corriges
      ] },
      { category: 'Projet', contents: [
        courses.webservices.projet ] }
    ]
  },

  projetl3: {
    title: '👨‍🎓 Projets Licence 3 Informatique',
    students: 'L3 Informatique',
    year: '2019-2020',
    resources: [
      { category: 'Projet', contents: [
        courses.projetl3.consignes,
        courses.projetl3.cours ] },
      { category: 'Réalisations', contents: [
        courses.projetl3.promo2020 ]}
    ]
  },

  gal: {
    title: '⚙️ Génie et Architecture Logicielle',
    students: 'M1 ISIDIS',
    year: '2017 - 2018',
    resources: [
      { category: 'Projet', contents: [
        courses.gal.projet ] }
    ]
  }

};

module.exports = { courses, teachings };
