const people = {
  me: {
    name: 'Florian Leprêtre',
    href: '/'
  },
  fonlupt: {
    name: 'Cyril Fonlupt',
    href: 'https://www-lisic.univ-littoral.fr/~fonlupt/index.html',
  },
  verel: {
    name: 'Sébastien Verel',
    href: 'http://www-lisic.univ-littoral.fr/~verel/',
  },
  marion: {
    name: 'Virginie Marion',
    href: 'http://www-lisic.univ-littoral.fr/~poty/',
  },
  tanaka: {
    name: 'Kiyoshi Tanaka',
    href: 'http://soar-rd.shinshu-u.ac.jp/profile/en.WUnUbpkh.html',
  },
  aguirre: {
    name: 'Hernàn Aguirre',
    href: 'http://soar-rd.shinshu-u.ac.jp/profile/en.gNDpbpkh.html',
  },
  armas: {
    name: 'Rolando Armas',
    href: 'https://www.researchgate.net/profile/Rolando_Armas2',
  },
  dehos: {
    name: 'Julien Dehos',
    href: 'http://julien.dehos.free.fr/',
  },
  teytaud: {
    name: 'Fabien Teytaud',
    href: 'http://www-lisic.univ-littoral.fr/~teytaud/',
  },
  ramat: {
    name: 'Éric Ramat',
    href: 'http://www-lisic.univ-littoral.fr/~ramat/',
  },
  dubois: {
    name: 'Amaury Dubois',
    href: 'https://amaury-dubois.herokuapp.com/',
  }
};

module.exports = { people };