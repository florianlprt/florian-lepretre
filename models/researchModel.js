const { people } = require('./peopleModel');

const research = {

  phd: {
    title: 'PhD thesis',
    items: [
      { 
        title: 'Méta-Modélisation, Simulation et Optimisation de Flux Urbains',
        href: 'https://florianlprt.gitlab.io/flolep-phd/main.pdf',
        pills: ['LISIC', '2020'],
        people: {
          title: 'Supervisors',
          items: [ people.fonlupt, people.verel, people.marion ]
        },
        more: 'Presentation: <a href="https://florianlprt.gitlab.io/flolep-phd/">Slides</a>'
      }
    ]
  },

  journal: {
    title: 'Journal articles with review committee',
    items: [
      { 
        title: 'Fitness landscapes analysis and adaptive algorithms design for traffic lights optimization on SIALAC benchmark',
        href: 'https://www.sciencedirect.com/science/article/abs/pii/S1568494619306507',
        pills: ['Applied Soft Computing', '2019'],
        people: {
          title: 'Authors',
          items: [ people.me, people.fonlupt, people.verel, people.marion, people.armas, people.aguirre, people.tanaka ]
        }
      }
    ]
  },

  conferences: {
    title: 'International conferences with review committee',
    items: [
      {
        title: 'Combinatorial Surrogate-Assisted Optimization for Bus Stops Spacing Problems',
        href: 'https://hal.archives-ouvertes.fr/hal-02319558',
        pills: ['EA', 'October 29-30, 2019', 'Mulhouse, France'],
        people: {
          title: 'Authors',
          items: [ people.me, people.fonlupt, people.verel, people.marion ]
        }
      },
      {
        title: 'Walsh functions as surrogate model for pseudo-boolean optimization problems',
        href: 'https://hal.archives-ouvertes.fr/hal-02190141',
        pills: ['GECCO', 'July 13-17, 2019', 'Prague, Czech Republic'],
        people: {
          title: 'Authors',
          items: [ people.me, people.fonlupt, people.verel, people.marion ]
        }
      },
      {
        title: 'SIALAC Benchmark: On the design of adaptive algorithms for traffic lights problems',
        href: 'https://hal.archives-ouvertes.fr/hal-01767696v1',
        pills: ['GECCO', 'July 15-19, 2018', 'Kyoto, Japan'],
        people: {
          title: 'Authors',
          items: [ people.me, people.fonlupt, people.verel, people.marion ]
        }
      },
      {
        title: 'Multi-armed bandit for stratified sampling : Application to numerical integration',
        href: 'https://hal.archives-ouvertes.fr/hal-01660617v1',
        pills: ['TAAI', 'Decembre 1-3, 2017', 'Taipei, Taiwan'],
        people: {
          title: 'Authors',
          items: [ people.me, people.dehos, people.teytaud ]
        }
      }
    ]
  },

  workshops: {
    title: 'National workshops',
    items: [
      {
        title: 'Algorithme UCB pour la synthèse d\'image et l\'optimisation numérique',
        href: '/downloads/research/workshop_ia_lisic_2018.pdf',
        pills: ['June 18, 2018', 'LISIC, France'],
        people: {
          title: 'Authors',
          items: [ people.me, people.fonlupt, people.verel, people.marion, people.dehos, people.teytaud ]
        }
      },
      {
        title: 'Base des fonctions de Walsh comme modèle de substitution pour l\'optimisation pseudo-booléenne',
        href: 'https://florianlprt.gitlab.io/gecco19_walsh/',
        pills: ['October 11, 2019', 'CRIStAL, France'],
        people: {
          title: 'Authors',
          items: [ people.me, people.fonlupt, people.verel, people.marion ]
        }
      },
    ]
  },

  seminars: {
    title: 'National seminars',
    items: [
      {
        title: 'Méta-modélisation, simulation et optimisation de flux urbains',
        href: 'https://florianlprt.gitlab.io/seminaire-lisic-02-04-20/#/intro',
        pills: ['April 2, 2020', 'LISIC, France'],
        people: {
          title: 'Authors',
          items: [ people.me, people.fonlupt, people.verel, people.marion ]
        }
      },
      {
        title: 'Apprentissage artificiel pour l\'intégration numérique',
        href: '/downloads/research/seminaire_pres_lepre_lisic_2017.pdf',
        pills: ['August 31, 2017', 'LISIC, France'],
        people: {
          title: 'Authors',
          items: [ people.me, people.dehos, people.teytaud ]
        }
      }
    ]
  }

};

module.exports = { research };
