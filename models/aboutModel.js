const { people } = require('./peopleModel');

const careers = {

  ater: {
    active: true,
    title: 'Temporary Lecturer and Research Assistant',
    pills: ['October 2020 → Now', 'ULCO', 'LISIC', 'France'],
    items: [
      { 
        title: 'French title',
        text: 'Attaché Temporaire d\'Enseignement et de Recherche (ATER)',
      }
    ]
  },

  phd: {
    title: 'Philosophiæ doctor candidate',
    pills: ['October 2017 → September', '2020', 'LISIC', 'France'],
    items: [
      { 
        title: 'French title',
        text: 'Méta-Modélisation, Simulation et Optimisation de Flux Urbains',
      },
      {
        title: 'Supervisors',
        people: [ people.fonlupt, people.verel, people.marion ]
      }
    ]
  },

  internship: {
    title: 'Internship in artificial intelligence and image synthesis',
    pills: ['April 2017 → September 2017', 'LISIC', 'France'],
    items: [
      { 
        title: 'French title',
        text: 'Implémentation d\'algorithmes d\'intelligence artificielle pour la synthèse d\'image',
      },
      {
        title: 'Supervisors',
        people: [ people.dehos, people.teytaud ]
      }
    ]
  },

  tutoring: {
    title: 'Student tutoring in algorithmics',
    pills: ['2014 → 2015', 'ULCO', 'France'],
    items: []
  }

};

const educations = {

  phd: {
    active: true,
    title: 'PhD in Computer Science',
    pills: ['2020', 'ULCO', 'France'],
    items: [
      {
        title: 'French title',
        text: '	Doctorat en Sciences et Technologies de l\'Information et de la Communication, spécialité Informatique.',
      }
    ]
  },

  master: {
    title: 'Master Degree in Computer Science',
    pills: ['2017', 'ULCO', 'France'],
    items: [
      {
        title: 'French title',
        text: 'Master Informatique, spécialité Ingénierie des Systèmes d\'Information Distrubués (ISIDis)',
      },
      {
        title: 'Honors',
        text: 'Major'
      }
    ]
  },

  licence: {
    title: 'Bachelor Degree in Computer Science',
    pills: ['2015', 'ULCO', 'France'],
    items: [
      {
        title: 'French title',
        text: 'Licence Informatique',
      },
      {
        title: 'Honors',
        text: 'Major'
      }
    ]
  }

};

module.exports = { careers, educations };
