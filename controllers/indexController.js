var aboutModel = require('../models/aboutModel');
var researchModel = require('../models/researchModel');
var portfolioModel = require('../models/portfolioModel');

// Display index page
exports.index = function(req, res) {
  res.render('page', { title: 'Home', view: 'pages/home' });
};

// Display about page
exports.about = function(req, res) {
  res.render('page', {
    title: 'About',
    view: 'pages/about',
    careers: aboutModel.careers,
    educations: aboutModel.educations
  });
};

// Display research page
exports.research = function(req, res) {
  res.render('page', {
    title: 'Research',
    view: 'pages/research',
    research: researchModel.research
  });
};

// Display portfolio page
exports.portfolio = function(req, res) {
  res.render('page', {
    title: 'Portfolio',
    view: 'pages/portfolio',
    portfolio: portfolioModel.portfolio
  });
};

// Display contact page
exports.contact = function(req, res) {
  res.render('page', { title: 'Contact', view: 'pages/contact' });
};
