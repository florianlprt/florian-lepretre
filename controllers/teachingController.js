var basicAuth = require('basic-auth');

var teachingModel = require('../models/teachingModel');

// Auth
const auth = function (req, res, next) {
  const user = basicAuth(req);
  const { topic, lesson } = req.params;
  const { pass } = teachingModel.courses[topic][lesson];
  if (user && user.pass === pass) {
    next();
  } else {
    res.set('WWW-Authenticate', 'Basic realm=Authorization Required');
    res.sendStatus(401);
  }
};

// Check lesson
exports.check = function(req, res, next) {
  const { topic, lesson } = req.params;
  if (!(topic in teachingModel.courses &&
      lesson in teachingModel.courses[topic])) {
    res.redirect('/teaching');
  } else if (teachingModel.courses[topic][lesson].pass) {
    auth(req, res, next);
  } else {
    next();
  }
};

// Display teaching page
exports.teaching = function(req, res) {
  res.render('page', {
    title: 'Teaching',
    view: 'pages/teaching',
    teachings: teachingModel.teachings });
};

// Display lesson page
exports.lesson = function(req, res) {
  const { topic, lesson } = req.params;
  res.render('lesson', {
    lesson,
    topic: teachingModel.teachings[topic],
    ...teachingModel.courses[topic][lesson] });
};

// Display slides page
exports.slides = function(req, res) {
  const { topic, lesson } = req.params;
  const { title: teachingTitle } = teachingModel.teachings[topic];
  res.render('slides', {
    lesson,
    teachingTitle,
    ...teachingModel.courses[topic][lesson] });
};
