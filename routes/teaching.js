var express = require('express');
var router = express.Router();

var controller = require('../controllers/teachingController');

// GET teaching
router.get('/', controller.teaching);

// GET any lesson format (check existence)
router.get('/:topic/:lesson*', controller.check);

// GET lesson
router.get('/:topic/:lesson', controller.lesson);

// GET slides
router.get('/:topic/:lesson/slides', controller.slides);

module.exports = router;
