var express = require('express');
var router = express.Router();

var teachingController = require('../controllers/teachingController');

// GET private teaching resources
router.get('/:topic/:lesson/*', teachingController.check);

module.exports = router;
