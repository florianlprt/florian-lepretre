var express = require('express');
var router = express.Router();

var controller = require('../controllers/indexController');

// GET index
router.get('/', controller.index);

// GET index
router.get('/about', controller.about);

// GET research
router.get('/research', controller.research);

// GET portolio
router.get('/portfolio', controller.portfolio);

// GET contact
router.get('/contact', controller.contact);

module.exports = router;
